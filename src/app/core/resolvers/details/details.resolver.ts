import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Champion } from 'src/app/models/champion';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { CoreState } from '../../stores/core/core.state';
import { GetChampionById } from '../../stores/core/core.actions';

@Injectable({
  providedIn: 'root'
})
export class DetailsResolver implements Resolve<any> {
  @Select(CoreState.selectedChampion) selectedChampion$: Observable<Champion>;
  selectedChampion: Champion;
  constructor(private store: Store) {
    this.selectedChampion$.subscribe(d => (this.selectedChampion = d));
  }

  resolve(route: ActivatedRouteSnapshot) {
    const id = route.params.id;

    if (!this.selectedChampion || this.selectedChampion.id !== id) {
      this.store.dispatch(new GetChampionById(id));
    }
  }
}
