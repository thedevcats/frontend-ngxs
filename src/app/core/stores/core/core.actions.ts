export class GetChampions {
  static readonly type = '[Core] Get Champions';
}

export class GetChampionById {
  static readonly type = '[Core] Get Champion by ID';
  constructor(public id: string) {}
}
