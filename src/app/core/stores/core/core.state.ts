import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { uniqBy as _uniqBy, map as _map } from 'lodash';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Champion } from 'src/app/models/champion';
import { ApiService } from '../../services/api/api.service';
import { GetChampions, GetChampionById } from './core.actions';

export class CoreStateModel {
  champions: Array<Champion>;
  selectedChampion: Champion;
}

@Injectable({
  providedIn: 'root'
})
@State<CoreStateModel>({
  name: 'core',
  defaults: {
    champions: [],
    selectedChampion: null
  }
})
export class CoreState {
  constructor(private apiSevice: ApiService, private router: Router, private store: Store) {
    // new GetChampions();
  }

  @Selector()
  static champions(state: CoreStateModel) {
    return state.champions;
  }

  @Selector()
  static selectedChampion(state: CoreStateModel) {
    return state.selectedChampion;
  }

  @Action(GetChampions)
  getChampions({ getState, patchState }: StateContext<CoreStateModel>) {
    const state = getState();

    return this.apiSevice.getAllChampions().pipe(
      tap((champions: Array<Champion>) => {
        patchState({
          champions
        });
      })
    );
  }

  @Action(GetChampionById)
  getChampion({ getState, patchState }: StateContext<CoreStateModel>, { id }: GetChampionById) {
    const state = getState();

    return this.apiSevice.getChampionById(id).pipe(
      tap((selectedChampion: Champion) => {
        patchState({
          selectedChampion
        });
      }),
      tap(() => {
        this.router.navigate(['champions', id]);
      })
    );
  }
}
