import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FakeBackendProvider } from './core/interceptors/fake-backend/fake-backend.interceptor';
import { ChampionDashboardComponent } from './components/champion-dashboard/champion-dashboard.component';
import { ChampionDetailsComponent } from './components/champion-details/champion-details.component';
import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { CoreState } from './core/stores/core/core.state';

@NgModule({
  declarations: [AppComponent, ChampionDashboardComponent, ChampionDetailsComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxsModule.forRoot([CoreState], { developmentMode: !environment.production }),
    NgxsReduxDevtoolsPluginModule.forRoot({ disabled: environment.production }),
    NgxsLoggerPluginModule.forRoot({ disabled: environment.production }),
    NgxsRouterPluginModule.forRoot()
  ],
  providers: [FakeBackendProvider],
  bootstrap: [AppComponent]
})
export class AppModule {}
