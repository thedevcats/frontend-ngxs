export interface Champion {
  id: string;
  name: string;
  title: string;
  tags: Array<string>;
  stats: {
    hp: number;
    mp: number;
    movespeed: number;
    armor: number;
    attackdamage: number;
    attackrange: number;
  };
  icon: string;
  sprite: {
    url: string;
  };
  description: string;
}
