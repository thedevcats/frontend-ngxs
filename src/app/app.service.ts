import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Champion } from './models/champion';
import { Select, Store } from '@ngxs/store';
import { CoreState } from './core/stores/core/core.state';
import { GetChampionById, GetChampions } from './core/stores/core/core.actions';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  @Select(CoreState.champions) champions$: Observable<Array<Champion>>;
  @Select(CoreState.selectedChampion) selectedChampion$: Observable<Champion>;

  constructor(private store: Store) {
    this.store.dispatch(new GetChampions());
  }

  navigateToDetails(id: string) {
    this.store.dispatch(new GetChampionById(id));
  }
}
